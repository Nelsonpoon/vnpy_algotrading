# vn.py框架的算法交易模块

<p align="center">
  <img src ="https://vnpy.oss-cn-shanghai.aliyuncs.com/vnpy-logo.png"/>
</p>

<p align="center">
    <img src ="https://img.shields.io/badge/version-1.0.1-blueviolet.svg"/>
    <img src ="https://img.shields.io/badge/platform-windows|linux|macos-yellow.svg"/>
    <img src ="https://img.shields.io/badge/python-3.7-blue.svg" />
    <img src ="https://img.shields.io/github/license/vnpy/vnpy.svg?color=orange"/>
</p>

## 说明

AlgoTrading是用于算法交易执行的功能模块，提供多种常用的智能交易算法：TWAP、Sniper、Iceberg、BestLimit等，支持通过UI界面、CSV批量导入、外部模块访问等多种调用方式。

## 安装

安装需要基于2.7.0版本以上的[VN Studio](https://www.vnpy.com)。

直接使用pip命令：

```
pip install vnpy_algotrading
```

下载解压后在cmd中运行

```
python setup.py install
```
